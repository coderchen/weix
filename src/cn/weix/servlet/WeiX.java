package cn.weix.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import cn.weix.bean.MsgBean;
import cn.weix.f.MsgFactory;
import cn.weix.f.MsgFactoryImpl;
import cn.weix.i.MsgStation;
import cn.weix.xml.ParseXml;

public class WeiX extends HttpServlet {
	
	Log log = LogFactory.getLog(WeiX.class);
	private static final long serialVersionUID = 1L;


	public WeiX() {
		super();
	}

	
	public void destroy() {
		super.destroy();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		String signature = request.getParameter("signature");
//		signature = signature == null ? " not avaliable value " : signature.trim();
//		
//		String timestamp = request.getParameter("timestamp");
//		timestamp = timestamp == null ? "not timestamp" :timestamp.trim();
//		
//		String nonce     = request.getParameter("nonce");
//		nonce = nonce == null ? "not a nonce " : nonce.trim();
//		
//		String echostr   = request.getParameter("echostr");
//		echostr = echostr == null ? "not echostr " : echostr.trim();
//		
//		String token = "chenmianyu".trim();
//		
//		String[] arr = {token,timestamp,nonce};
//		
//		Arrays.sort(arr);
//		String bigStr = arr[0]+arr[1]+arr[2];
//		 // SHA1加密
//        String digest = new SHA1().getDigestOfString(bigStr.getBytes()).toLowerCase();
//        // 确认请求来至微信
//        if (digest.equals(signature)) {
//            response.getWriter().print(echostr);
//        }else{
//        	response.getWriter().print("no pass!");
//        }
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		String xml = "";
		try {
			 SAXReader xmlReader = new SAXReader();
			 java.io.InputStream in = request.getInputStream();
			 Document doc = xmlReader.read(in);
			 
			 MsgBean mBean = ParseXml.parseXml(doc);	
			//关注定阅摧送事件
			if(mBean != null && "subscribe".equals(mBean.getEvent())){
				xml = getSubscript(mBean);
				
			}	
		
			if(xml == null || "".equals(xml)) return;
			log.info("----"+xml);
			PrintWriter out = response.getWriter();
			out.print(xml);
			out.flush();
			out.close();
		} catch (Exception e) {
			log.info("sendMsg = "+e.getMessage());
		}
	}
	//加关注时发送默认消息
	private String getSubscript(MsgBean m){
		MsgFactory mf = new MsgFactoryImpl();
		MsgStation ms = mf.loadNews();
		return ms.sendXml(m);
	}
	
	
	public void init() throws ServletException {
		
	}
}

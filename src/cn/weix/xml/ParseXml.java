package cn.weix.xml;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;

import cn.weix.bean.MsgBean;

public class ParseXml {
	static Log log = LogFactory.getLog(ParseXml.class);

	static DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

	public static MsgBean parseXml(Document doc) {
		try {
			Element root = doc.getRootElement();
			MsgBean mbean = new MsgBean();
			mbean.setToUserName(root.elementText("ToUserName"));
			mbean.setFromUserName(root.elementText("FromUserName"));
			mbean.setCreateTime(root.elementText("CreateTime"));
			mbean.setMsgType(root.elementText("MsgType"));
			mbean.setEvent(root.elementText("Event"));
			return mbean;
		} catch (Exception e) {
			return new MsgBean();
		}
	}
}

package cn.weix.i;


import org.dom4j.Document;

import cn.weix.bean.MsgBean;

public interface MsgStation {
	public String sendXml(MsgBean m);
	public MsgBean parseXml(Document doc);
}

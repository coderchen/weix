package cn.weix.f;

import org.w3c.dom.Document;

import cn.weix.i.MsgStation;
import cn.weix.t.Event;
import cn.weix.t.News;
import cn.weix.t.Text;

public class MsgFactoryImpl implements MsgFactory {

	public MsgStation loadEvent() {
		return new Event();
	}

	public MsgStation loadText() {
		return new Text();
	}

	public MsgStation parseXml(Document doc) {
		return null;
	}

	public MsgStation loadNews() {
		return new News();
	}

}

package cn.weix.f;

import org.w3c.dom.Document;

import cn.weix.i.MsgStation;

public interface MsgFactory {
	public MsgStation loadText();
	public MsgStation loadEvent();
	public MsgStation loadNews();
	
	public MsgStation parseXml(Document doc);

}

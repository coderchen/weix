package cn.weix.t;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import cn.weix.a.AbstractText;
import cn.weix.bean.MsgBean;

public class Text extends AbstractText {
	
	public String sendXml(MsgBean m){
		StringBuffer xml = new StringBuffer();
		xml.append("<xml>");
		xml.append("<ToUserName>"+m.getToUserName()+"</ToUserName>");
		xml.append("<FromUserName>"+m.getFromUserName()+"</FromUserName>");
		xml.append("<CreateTime>"+System.currentTimeMillis()+"</CreateTime>");
		xml.append("<MsgType>text</MsgType>");
		xml.append("<Content>"+m.getContent()+"</Content>");
		xml.append("</xml>");
		return xml.toString();
	}
	//解析XML输入流
	public MsgBean parseXml(Document doc) {
		SAXReader reader =new SAXReader();
		try {
			Document document = (Document) reader.read(new File("c:\\c.xml"));
			Element root = document.getRootElement();
			MsgBean mbean = new MsgBean();
			mbean.setToUserName(root.elementText("ToUserName"));
			mbean.setFromUserName(root.elementText("FromUserName"));
			mbean.setMsgType(root.elementText("MsgType"));
			mbean.setCreateTime(root.elementText("CreateTime"));
			mbean.setContent(root.elementText("Content"));
			mbean.setMsgId(root.elementText("MsgId"));
			return mbean;
			
		} catch (DocumentException e) {
			return new MsgBean();
		}
	}
}

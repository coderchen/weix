package cn.weix.t;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import cn.weix.a.AbstractEvent;
import cn.weix.bean.MsgBean;
import cn.weix.f.MsgFactory;
import cn.weix.f.MsgFactoryImpl;
import cn.weix.i.MsgStation;

public class Event extends AbstractEvent{

	public String sendXml(MsgBean m) {
		MsgFactory mF = new MsgFactoryImpl();
		MsgStation ms = mF.loadNews();
		return ms.sendXml(m);
	}

	public MsgBean parseXml(Document doc) {
		SAXReader reader =new SAXReader();
		try {
			Document document = (Document) reader.read(new File("c:\\c.xml"));
			Element root = document.getRootElement();
			MsgBean mbean = new MsgBean();
			mbean.setToUserName(root.elementText("ToUserName"));
			mbean.setFromUserName(root.elementText("FromUserName"));
			mbean.setCreateTime(root.elementText("CreateTime"));
			mbean.setMsgType(root.elementText("MsgType"));
			mbean.setEvent(root.elementText("Event"));
			return mbean;
			
		} catch (DocumentException e) {
			return new MsgBean();
		}
	}

}

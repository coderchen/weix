package cn.weix.t;

import org.dom4j.Document;

import cn.weix.a.AbstractNews;
import cn.weix.bean.MsgBean;

public class News extends AbstractNews{

	public MsgBean parseXml(Document doc) {
		return null;
	}

	public String sendXml(MsgBean m) {
		StringBuffer xml = new StringBuffer();
		xml.append("<xml>");
		xml.append("<ToUserName>"+m.getFromUserName()+"</ToUserName>");
		xml.append("<FromUserName>"+m.getToUserName()+"</FromUserName>");
		xml.append("<CreateTime>"+m.getCreateTime()+"</CreateTime>");
		xml.append("<MsgType>news</MsgType>");
		xml.append("<ArticleCount>5</ArticleCount>");
		xml.append("<Articles>");
		xml.append("<item>");
		xml.append("<Title>你好!新街口!</Title>");
		xml.append("<Description>Description</Description>");
		xml.append("<PicUrl>http://lwzxwx.duapp.com/img/head.jpg</PicUrl>");
		xml.append("<Url>http://www.njzysc.com/index.html</Url>");
		xml.append("</item>");
		xml.append("<item>");
		xml.append("<Title>中央商场(点我有惊喜哦)</Title>");
		xml.append("<Description>点我有惊喜哦..</Description>");
		xml.append("<PicUrl>http://lwzxwx.duapp.com/img/zysc.png</PicUrl>");
		xml.append("<Url>http://www.njzysc.com/index.html</Url>");
		xml.append("</item>");
		xml.append("<item>");
		xml.append("<Title>新百(哎!还是没有人点我)</Title>");
		xml.append("<Description></Description>");
		xml.append("<PicUrl>http://lwzxwx.duapp.com/img/xb.png</PicUrl>");
		xml.append("<Url>http://www.njxb.com/index.html</Url>");
		xml.append("</item>");
		xml.append("<item>");
		xml.append("<Title>德基广场(好开心,我被相中N-1次了,咯咯)</Title>");
		xml.append("<Description></Description>");
		xml.append("<PicUrl>http://lwzxwx.duapp.com/img/dj.png</PicUrl>");
		xml.append("<Url>http://dejiplaza.com/</Url>");
		xml.append("</item>");
		xml.append("<item>");
		xml.append("<Title>东方商城(我还真不相信,没人理我?!)</Title>");
		xml.append("<Description></Description>");
		xml.append("<PicUrl>http://lwzxwx.duapp.com/img/dfsc.png</PicUrl>");
		xml.append("<Url>http://www.njorient.com</Url>");
		xml.append("</item>");
		xml.append("</Articles>");
		xml.append("</xml>");		
		return xml.toString();
	}

}
